import React from 'react';
import CoinGecko from 'coingecko-api';
const coinGeckoClient = new CoinGecko();

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { favouriteList: [], result: [] }
    if (window.performance) {
      if (performance.navigation.type == 1) {
        if (window.localStorage.getItem('favouriteList') !== null) {
          let favouriteList = [];
          window.localStorage.setItem('favouriteList', JSON.stringify(favouriteList));
        }
      }
    }
  }

  async componentDidMount() {
    const params = {
      order: CoinGecko.ORDER.MARKET_CAP_DESC
    };
    const data = await coinGeckoClient.coins.markets({ params });
    this.setState({ result: data.data });

    if (window.localStorage.getItem('onFavourite') === null) {
      window.localStorage.setItem('onFavourite', "false");
    }
    this.setupBeforeUnloadListener();
  }

  saveFavourites = (key) => {
    if (this.state.favouriteList.includes(key)) {
      const index = this.state.favouriteList.indexOf(key);
      this.state.favouriteList.splice(index, 1);
      window.localStorage.setItem('favouriteList', JSON.stringify(this.state.favouriteList));
      console.log(this.state.favouriteList);
    }
    else {
      this.state.favouriteList.push(key);
      window.localStorage.setItem('favouriteList', JSON.stringify(this.state.favouriteList));
      console.log(this.state.favouriteList);
    }
  }

  formatPercent = number =>
    `${new Number(number).toFixed(2)}%`

  formatDollar = (number, maximumSignificantDigits) =>
    new Intl.NumberFormat(
      'en-US',
      {
        style: 'currency',
        currency: 'USD',
        maximumSignificantDigits
      })
      .format(number);

  toggleFavourite = () => {
    if (window.localStorage.getItem('onFavourite') == "true") {
      window.localStorage.setItem('onFavourite', "false");
    }
    else {
      window.localStorage.setItem('onFavourite', "true");
    }
    window.open("/")
  }

  doSomethingBeforeUnload = () => {
    window.localStorage.setItem('onFavourite', "false");
  }

  setupBeforeUnloadListener = () => {
    window.addEventListener("beforeunload", (ev) => {
      ev.preventDefault();
      return this.doSomethingBeforeUnload();
    });
  };

  render() {
    if (window.localStorage.getItem('onFavourite') === "false" || window.localStorage.getItem('onFavourite') === null) {
      return (
        <div className={{ display: "flex", flex: "column", justifyContent: "center", alignItems: "center" }}>
          <h1>Top 20 Coins By Market Capitalization</h1>
          <input type="submit" value="Favourite" onClick={() => this.toggleFavourite()} />

          <table className="table">
            <style>{"table, th, td {border:1px solid black;}"}</style>
            <thead>
              <tr>
                <th>Favourites</th>
                <th>Symbol</th>
                <th>24H Change</th>
                <th>Price</th>
                <th>Market cap</th>
                <th>7 Day Chart</th>
              </tr>
            </thead>
            <tbody>
              {this.state.result.slice(0, 20).map(coin => (
                <tr key={coin.id}>
                  <td><input type="checkbox" onClick={() => this.saveFavourites(coin)} /></td>
                  <td>
                    <img
                      src={coin.image}
                      style={{ width: 25, height: 25, marginRight: 10 }}
                    />
                    {coin.symbol.toUpperCase()}
                  </td>
                  <td>
                    <span
                      className={coin.price_change_percentage_24h > 0 ? (
                        'text-success'
                      ) : 'text-danger'}
                    >
                      {this.formatPercent(coin.price_change_percentage_24h)}
                    </span>
                  </td>
                  <td>{this.formatDollar(coin.current_price, 20)}</td>
                  <td>{this.formatDollar(coin.market_cap, 12)}</td>
                  <td><img src={"http://34.87.166.141:8000/" + coin.id} /></td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )
    }
    else {
      let favouriteList = [];
      if (window.localStorage.getItem('favouriteList') !== null) {
        favouriteList = JSON.parse(window.localStorage.getItem('favouriteList'));
      }
      return (
        <div className={{ display: "flex", flex: "column", justifyContent: "center", alignItems: "center" }}>
          <h1>Favourite Coins By Market Capitalization</h1>

          <table className="table">
            <style>{"table, th, td {border:1px solid black;}"}</style>
            <thead>
              <tr>
                <th>Symbol</th>
                <th>24H Change</th>
                <th>Price</th>
                <th>Market cap</th>
                <th>7 Day Chart</th>
              </tr>
            </thead>
            <tbody>
              {favouriteList.map(coin => (
                <tr key={coin.id}>
                  <td>
                    <img
                      src={coin.image}
                      style={{ width: 25, height: 25, marginRight: 10 }}
                    />
                    {coin.symbol.toUpperCase()}
                  </td>
                  <td>
                    <span
                      className={coin.price_change_percentage_24h > 0 ? (
                        'text-success'
                      ) : 'text-danger'}
                    >
                      {this.formatPercent(coin.price_change_percentage_24h)}
                    </span>
                  </td>
                  <td>{this.formatDollar(coin.current_price, 20)}</td>
                  <td>{this.formatDollar(coin.market_cap, 12)}</td>
                  <td><img src={"http://34.87.166.141:8000/" + coin.id} /></td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )
    }
  }
}

export default App;
