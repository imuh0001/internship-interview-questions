var vega = require('vega');
var fs = require('fs');
let express = require('express');

let app = express();
app.listen(8000);

const fetch = require('node-fetch');

var stackedBarChartSpec = require('./stacked-bar-chart.spec.json');
var spec = JSON.stringify(stackedBarChartSpec);
spec = spec.slice(0, spec.length - 1);

let url = "https://api.coingecko.com/api/v3/coins/";
let parameters = "?sparkline=true&localization=false&community_data=false&developer_data=false&tickers=false";
let settings = { method: "Get" };

let format1 = `,"data": [{"name": "table","values": [`;
let format2 = `]}]}`;
var data;
var data_format = "";

let format3 = `,"scales": [{"name": "x","type": "point","range": "width","domain": {"data": "table","field": "x"}},{"name": "y","type": "linear","range": "height","nice": true,"zero": false,"domain":[`;
let format4 = `]},{"name": "color","type": "ordinal","range": "category","domain": {"data": "table","field": "c"}}]`;

var coins = ['bitcoin', 'ethereum', 'tether', 'ripple', 'bitcoin-cash', 'binancecoin', 'chainlink', 'polkadot', 'cardano', 'litecoin', 'bitcoin-cash-sv', 'crypto-com-chain', 'usd-coin', 'eos', 'monero', 'tron', 'okb', 'tezos', 'stellar', 'cosmos', 'wrapped-bitcoin', 'neo', 'cdai', 'leo-token', 'dash', 'iota', 'huobi-token', 'binance-usd', 'dai', 'nem'];
var coinView;

function request() {
    coinView = {};
    for (let i = 0; i < coins.length; i++) {
        data_format = "";
        let finalUrl = url + coins[i] + parameters;
        fetch(finalUrl, settings)
            .then(res => res.json())
            .then((json) => {
                data = json['market_data']['sparkline_7d']["price"];
                let min = Math.min(...data)
                let max = Math.max(...data)
                var format;
                data_format = "";
                for (let i = 0; i < data.length; i++) {
                    if (i == data.length - 1) {
                        format = `{"x": ${i}, "y":${data[i]}, "c":0}`;
                    }
                    else {
                        format = `{"x": ${i}, "y":${data[i]}, "c":0},`;
                    }
                    data_format += format;
                }

                let scaleFormat = format3 + min + ',' + max + format4;
                let json_spec = spec + scaleFormat + format1 + data_format + format2;

                fs.writeFile(coins[i] + ".json", json_spec, 'utf8', function (err) {
                    if (err) {
                        console.log(coins[i] + ": An error occured while writing JSON Object to File.");
                        return console.log(err);
                    }
                    console.log(coins[i] + " JSON file has been saved.");
                    let coinSpec = require('./' + coins[i] + '.json');

                    var view = new vega
                        .View(vega.parse(coinSpec))
                        .renderer('none')
                        .initialize();

                    view
                        .toImageURL('png')
                        .then(function (url) {
                            var base64Data = url.replace(/^data:image\/png;base64,/, "");

                            fs.writeFile(coins[i] + ".png", base64Data, 'base64', function (err) {
                                console.log(err);
                            });
                        })
                        .catch(function (err) {
                            console.log("Error creating link to file:")
                            console.error(err)
                        });

                });
            });

    }
}

setInterval(request, 15000);

app.get('/:id', (req, res) => {
    let coin = req.params.id;
    res.sendFile('/home/izadimrang7/Generate-Sparkline/' + coin + '.png');
});